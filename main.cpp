#include "mainwindow.h"

#include <QApplication>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
#include <QPainter>

#include <QDir>

QImage blurImage(QImage source)
{
    if (source.isNull()) return QImage();
    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(source));

    auto *blur = new QGraphicsBlurEffect;
    blur->setBlurRadius(8);
    item.setGraphicsEffect(blur);
    scene.addItem(&item);
    QImage result (source.size(), QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    QPainter painter (&result);
    scene.render(&painter, QRectF(),
                 QRectF(0,0,source.width(),source.height()));
    return result;
}

void processSingleImage(QString sourceFile, QString destFile)
{
   auto blured = blurImage(QImage(sourceFile));
   blured.save(destFile);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    processSingleImage("_","_");
    return a.exec();
}
#include<main.moc>
